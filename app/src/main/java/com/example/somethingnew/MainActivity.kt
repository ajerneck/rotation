package com.example.somethingnew

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment

import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview

import com.example.somethingnew.ui.theme.SomethingNewTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SomethingNewTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                    val c = applicationContext.applicationContext
                    Greeting(c)

                }
            }
        }
    }
}

fun launchPage(context: Context) {

    val url = Uri.parse("https://www.wikipedia.org/wiki/Special:Random")
    val intent = Intent(Intent.ACTION_VIEW, url)
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    context.startActivity(intent)
//    if (intent.resolveActivity(context.packageManager) != null) {
//        context.startActivity(intent)
//    }

}

@Composable
fun Greeting(context: Context, modifier: Modifier = Modifier) {
    Column(modifier = modifier
        , horizontalAlignment = Alignment.CenterHorizontally
        , verticalArrangement = Arrangement.Bottom
    ) {
        Button(onClick = {
            launchPage(context)

        }) {
            Text("Go!")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    SomethingNewTheme {
        val c = LocalContext.current
        Greeting(c)

    }
}